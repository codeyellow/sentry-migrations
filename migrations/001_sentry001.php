<?php
namespace Fuel\Migrations;

class Sentry001
{

    function up()
    {
        \DBUtil::create_table('groups', array(
            'id' => array(
                'type' => 'int',
                'constraint' => 10,
                'auto_increment' => true,
                'unsigned' => true 
            ),
            'name' => array(
                'type' => 'varchar',
                'constraint' => 255
            ),
            'permissions' => array(
                'type' => 'text',
                'null' => true
            ),
            'created_at' => array(
                'type' => 'timestamp',
                'default' => '0000-00-00 00:00:00'
            ),
            'updated_at' => array(
                'type' => 'timestamp',
                'default' => '0000-00-00 00:00:00'
            ) 
        ), array(
            'id' 
        ));

        \DBUtil::create_index('groups', 'name', 'groups_name_unique', 'UNIQUE');

        \DBUtil::create_table('throttle', array(
            'id' => array(
                'type' => 'int',
                'constraint' => 10,
                'auto_increment' => true,
                'unsigned' => true 
            ),
            'user_id' => array(
                'type' => 'int',
                'constraint' => 10,
                'unsigned' => true 
            ),
            'ip_address' => array(
                'type' => 'int',
                'constraint' => 255,
                'null' => true,
                'default' => \DB::expr('NULL')
            ),
            'attempts' => array(
                'type' => 'int',
                'constraint' => 11,
                'default' => 0
            ),
            'suspended' => array(
                'type' => 'tinyint',
                'constraint' => 4,
                'default' => 0
            ),
            'banned' => array(
                'type' => 'tinyint',
                'constraint' => 4,
                'default' => 0
            ),
            'last_attempt_at' => array(
                'type' => 'timestamp',
                'null' => true,
                'default' => \DB::expr('NULL')
            ),
            'suspended_at' => array(
                'type' => 'timestamp',
                'null' => true,
                'default' => \DB::expr('NULL')
            ),
            'banned_at' => array(
                'type' => 'timestamp',
                'null' => true,
                'default' => \DB::expr('NULL')
            )
        ), array(
            'id' 
        ));

        \DBUtil::create_table('users', array(
            'id' => array(
                'type' => 'int',
                'constraint' => 10,
                'auto_increment' => true,
                'unsigned' => true 
            ),
            'email' => array(
                'type' => 'varchar',
                'constraint' => 255,
            ),
            'password' => array(
                'type' => 'varchar',
                'constraint' => 255,
            ),
            'permissions' => array(
                'type' => 'text',
                'null' => true
            ),
            'activated' => array(
                'type' => 'tinyint',
                'constraint' => 4,
                'default' => 0
            ),
            'activation_code' => array(
                'type' => 'varchar',
                'constraint' => 255,
                'null' => true,
                'default' => \DB::expr('NULL')
            ),
            'activated_at' => array(
                'type' => 'varchar',
                'constraint' => 255,
                'null' => true,
                'default' => \DB::expr('NULL')
            ),
            'last_login' => array(
                'type' => 'varchar',
                'constraint' => 255,
                'null' => true,
                'default' => \DB::expr('NULL')
            ),
            'persist_code' => array(
                'type' => 'varchar',
                'constraint' => 255,
                'null' => true,
                'default' => \DB::expr('NULL')
            ),
            'reset_password_code' => array(
                'type' => 'varchar',
                'constraint' => 255,
                'null' => true,
                'default' => \DB::expr('NULL')
            ),
            'first_name' => array(
                'type' => 'varchar',
                'constraint' => 255,
                'null' => true,
                'default' => \DB::expr('NULL')
            ),
            'last_name' => array(
                'type' => 'varchar',
                'constraint' => 255,
                'null' => true,
                'default' => \DB::expr('NULL')
            ),
            'created_at' => array(
                'type' => 'timestamp',
                'default' => '0000-00-00 00:00:00'
            ),
            'updated_at' => array(
                'type' => 'timestamp',
                'default' => '0000-00-00 00:00:00'
            )
        ), array(
            'id' 
        ));

        \DBUtil::create_index('users', 'email', 'users_email_unique', 'UNIQUE');
        \DBUtil::create_index('users', 'activation_code', 'users_activation_code_index');
        \DBUtil::create_index('users', 'reset_password_code', 'users_reset_password_code_index');

        \DBUtil::create_table('users_groups', array(
            'id' => array(
                'type' => 'int',
                'constraint' => 10,
                'auto_increment' => true,
                'unsigned' => true 
            ),
            'user_id' => array(
                'type' => 'int',
                'constraint' => 10,
                'unsigned' => true 
            ),
            'group_id' => array(
                'type' => 'int',
                'constraint' => 10,
                'unsigned' => true 
            ),
        ), array(
            'id' 
        ));
    }

    function down()
    {
        \DBUtil::drop_table('users_groups');
        \DBUtil::drop_table('users');
        \DBUtil::drop_table('groups');
        \DBUtil::drop_table('throttle');
    }
}